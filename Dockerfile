FROM golang:alpine

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY *.go ./

RUN go build -o /clientp2p

EXPOSE 13768

CMD [ "/clientp2p" ]