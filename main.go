package main

import (
	"fmt"
	"net"
	"strings"

	"github.com/vmihailenco/msgpack/v5"
)

type NetworkCommand struct {
	Command string
	Payload []byte
}

type StartPeerConnectionCommand struct {
	Peers []string
}

func main() {
	conn, err := net.Dial("tcp", "5.189.145.4:13768")
	if err != nil {
		fmt.Println(err)
	}

	conn.Write([]byte("getselfaddress"))

	selfAddress := make([]byte, 4096)
	n, readerError := conn.Read(selfAddress)
	if readerError != nil {
		fmt.Println(readerError)
	}

	selfAddressString := string(selfAddress[0:n])
	fmt.Println("Self address ", selfAddressString, " ", n)
	localServerPort := ":" + strings.Split(conn.LocalAddr().String(), ":")[1]

	conn.Write([]byte("startpunching"))
	for {

		buffer := make([]byte, 4096)

		n, readerError = conn.Read(buffer)

		if readerError != nil {
			fmt.Println(readerError)
			break
		}

		command := DeserializeNetworkCommand(buffer)

		if command.Command == "startpunching" {
			fmt.Println("Started punching...")
			peerConnectionCommand := DeserializeStartPeerConnectionCommand(command.Payload)
			peers := peerConnectionCommand.Peers
			fmt.Println("Connection to peers...", peers)
			if len(peers) > 0 {
				conn.Close()

				startListener(localServerPort)
				for _, peerAddress := range peers {
					fmt.Println("Attempting to connect to ", peerAddress)
					/*remoteDecomposedAddress := strings.Split(peerAddress, ":")
					remotePort, parseError := strconv.Atoi(remoteDecomposedAddress[len(remoteDecomposedAddress)-1])
					if parseError != nil {
						fmt.Println(parseError)
					}
					remoteAddressStr := remoteDecomposedAddress[0]

					localDecomposedAddress := strings.Split(conn.LocalAddr().String(), ":")
					localPort, parseError := strconv.Atoi(localDecomposedAddress[len(localDecomposedAddress)-1])
					if parseError != nil {
						fmt.Println(parseError)
					}
					localAddressStr := localDecomposedAddress[0]

					peerConnection, peerConnectionError := net.DialTCP("tcp", &net.TCPAddr{
						IP:   net.ParseIP(localAddressStr),
						Port: localPort,
					}, &net.TCPAddr{
						IP:   net.ParseIP(remoteAddressStr),
						Port: remotePort,
					})*/
					peerConnection, peerConnectionError := net.Dial("tcp", peerAddress)
					if peerConnectionError != nil {
						fmt.Println("Connection failed ", peerConnectionError)
						continue
					}
					fmt.Println("Connected to ", peerConnection.RemoteAddr().String())
					break

				}
			}

		}
	}
	fmt.Println("Waiting for user to close console...")
	fmt.Scanln()
}

func startListener(listenAddress string) {
	listener, listenerErr := net.Listen("tcp", listenAddress)
	if listenerErr != nil {
		fmt.Println(listenerErr)
		return
	}
	fmt.Println("Now listening on ", listener.Addr().String())
	go func() {
		client, clientError := listener.Accept()

		if clientError != nil {
			fmt.Println(clientError)
		}

		fmt.Println("Client connected! ", client.RemoteAddr())
	}()

}

func (c *StartPeerConnectionCommand) Serialize() []byte {
	b, err := msgpack.Marshal(c)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func (c *NetworkCommand) Serialize() []byte {
	b, err := msgpack.Marshal(c)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func DeserializeStartPeerConnectionCommand(buffer []byte) *StartPeerConnectionCommand {
	command := StartPeerConnectionCommand{}
	err := msgpack.Unmarshal(buffer, &command)
	if err != nil {
		fmt.Println(err)
	}
	return &command
}

func DeserializeNetworkCommand(buffer []byte) *NetworkCommand {
	command := NetworkCommand{}
	err := msgpack.Unmarshal(buffer, &command)
	if err != nil {
		fmt.Println(err)
	}
	return &command
}
